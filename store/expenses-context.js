import { createContext, useReducer } from "react";

const DUMMY_EXPENSES = [
  {
    id: "e1",
    description: "A pair of shoes",
    amount: 59.99,
    date: new Date("2023-01-07"),
  },
  {
    id: "e2",
    description: "A pair of hats",
    amount: 10.99,
    date: new Date("2023-02-05"),
  },
  {
    id: "e3",
    description: "A book",
    amount: 21.2,
    date: new Date("2023-12-03"),
  },
  {
    id: "e4",
    description: "A cake",
    amount: 11.5,
    date: new Date("2023-11-17"),
  },
  {
    id: "e5",
    description: "A fender jazz",
    amount: 1500,
    date: new Date("2023-04-01"),
  },
  {
    id: "e6",
    description: "A warlrus",
    amount: 1.2,
    date: new Date("2022-12-31"),
  },
  {
    id: "e7",
    description: "A car",
    amount: 22001.2,
    date: new Date("2023-03-12"),
  },
  {
    id: "e8",
    description: "A fender precision",
    amount: 1500,
    date: new Date("2023-04-11"),
  },
  {
    id: "e9",
    description: "A milk",
    amount: 1.2,
    date: new Date("2022-05-31"),
  },
  {
    id: "e10",
    description: "A disk",
    amount: 21.2,
    date: new Date("2023-04-12"),
  },
];

export const ExpensesContext = createContext({
  expenses: [],
  addExpense: ({ description, amount, date }) => {},
  deleteExpense: (id) => {},
  updateExpense: (id, { descriptio, amount, date }) => {},
});

const expensesReducer = (state, action) => {
  switch (action.type) {
    case "ADD":
      const id = new Date().toString() + Math.random().toString();
      return [{ ...action.payload, id: id }, ...state];
    case "UPDATE":
      const updatableExpenseIndex = state.findIndex(
        (expense) => expense.id === action.payload.id
      );
      const updatableExpense = state[updatableExpenseIndex];
      const updatedItem = { ...updatableExpense, ...action.payload.data };
      const updatedExpenses = [...state];
      updatedExpenses[updatableExpenseIndex] = updatedItem;
      return updatedExpenses;
    case "DELETE":
      return state.filter((expense) => expense.id !== action.payload);
    default:
      return state;
  }
};

export default ExpensesContextProvider = ({ children }) => {
  const [expensesState, dispatch] = useReducer(expensesReducer, DUMMY_EXPENSES);

  const addExpense = (expenseData) =>
    dispatch({ type: "ADD", payload: expenseData });

  const deleteExpense = (id) => dispatch({ type: "DELETE", payload: id });

  const updateExpense = (id, expenseData) =>
    dispatch({ type: "UPDATE", payload: { id, data: expenseData } });

  const value = {
    expenses: expensesState,
    addExpense,
    deleteExpense,
    updateExpense,
  };

  return (
    <ExpensesContext.Provider value={value}>
      {children}
    </ExpensesContext.Provider>
  );
};
