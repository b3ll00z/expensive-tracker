## The Expensive Tracker

Another useful playground on react-native. The idea of this app is to keep track of your recorded expenses.
The exercise is taken from the RN course you can fine [here](https://github.com/academind/react-native-practical-guide-code/tree/08-practice-app)
